== Description ==

Simple lunar ball hack I've made as a proof of concept of Lunar Ball Editor.
It has all the original levels shrunk down and rotated 90 degrees to the left.

== How to patch ==

This is an IPS patch. Use any IPS patcher like Lunar IPS and apply it to one of the ROMs below.

== ROM information ==

ROM: Lunar Ball (Japan).nes (No-Intro)
CRC32: 4d483147
MD5: 26f1b77980a216767ea63c41397476e5
SHA-1: aa5c574a4743991a3523dfd78a39d782bede262a
SHA-256: 36aac165f903931e4a6048b4de65aa3d6668644204eb7ca30a259048eebfbb83

ROM: Lunar Ball (J).nes
CRC32: 83bdfe59
MD5: 5cf72484216bc6e0ad767b5164046874
SHA-1: e55f8ab397849fcca09379646aa487627f8a6189
SHA-256: dedc153c9ae425d348698c8f1ed6abf611a1efc69a75d23d808d7cbb9edfdd38
